import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class XTextField extends StatefulWidget {
  final String? id;
  final String label;
  final String? value;
  final String? hint;
  final String? Function(String?)? validator;
  final bool obscure;
  final bool enabled;
  final int? maxLength;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Function(String) onChanged;
  final Function(String)? onSubmitted;
  final bool readOnly;
  final bool onTap;

  const XTextField({
    Key? key,
    required this.label,
    this.id,
    this.value,
    this.validator,
    this.hint,
    this.maxLength,
    required this.onChanged,
    this.onSubmitted,
    this.obscure = false,
    this.enabled = true,
    this.prefixIcon,
    this.suffixIcon,
    this.readOnly = false,
    this.onTap = false,
  }) : super(key: key);

  @override
  State<XTextField> createState() => _XTextFieldState();
}

class _XTextFieldState extends State<XTextField> {
  TextEditingController textEditingController = TextEditingController();
  bool _showClearIcon = false;

  @override
  void initState() {
    textEditingController.text = widget.value ?? "";
    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  getValue() {
    return textEditingController.text;
  }

  @override
  setValue(value) {
    textEditingController.text = value;
  }

  @override
  resetValue() {
    textEditingController.text = "";
  }

  @override
  focus() {
    focusNode.requestFocus();
  }

  FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        readOnly: widget.readOnly,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        enabled: widget.enabled,
        controller: textEditingController,
        focusNode: focusNode,
        validator: widget.validator,
        maxLength: widget.maxLength,
        obscureText: widget.obscure,
        decoration: InputDecoration(
          labelText: widget.label,
          prefixIcon: (widget.prefixIcon != null)
              ? Icon(
                  widget.prefixIcon,
                )
              : null,
          labelStyle: GoogleFonts.raleway(
            textStyle: const TextStyle(
                fontSize: 15, fontWeight: FontWeight.bold, letterSpacing: 2),
          ),
          focusedBorder: const OutlineInputBorder(
              borderSide: BorderSide(color: Colors.blue),
              borderRadius: BorderRadius.all(Radius.circular(20))),
          border: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20))),
          suffixIcon: _showClearIcon
              ? IconButton(
                  icon: const Icon(Icons.clear),
                  onPressed: () {
                    textEditingController.clear();
                    setState(() {
                      _showClearIcon = false;
                    });
                  },
                )
              : null,
          helperText: widget.hint,
        ),
        onTap: (widget.onTap)
            ? () async {
                TimeOfDay? pickedTime = await showTimePicker(
                  initialTime: TimeOfDay.now(),
                  context: context, //context of current state
                );

                if (pickedTime != null) {
                  print(pickedTime.format(context)); //output 10:51 PM
                  setState(() {
                    textEditingController.text =
                        pickedTime.format(context).toString();
                    if (textEditingController.text.isNotEmpty) {
                      _showClearIcon = true;
                    } else {
                      _showClearIcon = false;
                    }
                    widget.onChanged(textEditingController.text);
                  });
                } else {
                  print("Time is not selected");
                }

                // DateTime? pickedDate = await showDatePicker(
                //     context: context,
                //     initialDate: DateTime.now(), //get today's date
                //     firstDate: DateTime(
                //         2000), //DateTime.now() - not to allow to choose before today.
                //     lastDate: DateTime(2101));

                // if (pickedDate != null) {
                //   print(
                //       pickedDate); //get the picked date in the format => 2022-07-04 00:00:00.000
                //   String formattedDate = DateFormat('yyyy-MM-dd HH:mm:ss').format(
                //       pickedDate); // format date in required form here we use yyyy-MM-dd that means time is removed
                //   print(
                //       formattedDate); //formatted date output using intl package =>  2022-07-04
                //   //You can format date as per your need

                //   setState(() {
                //     textEditingController.text =
                //         formattedDate; //set foratted date to TextField value.
                //   });
                // } else {
                //   print("Batas waktu pembayaran wajib diisi!");
                // }
              }
            : () {},
        onChanged: (value) {
          setState(() {
            if (value.isNotEmpty) {
              _showClearIcon = true;
            } else {
              _showClearIcon = false;
            }
          });
          widget.onChanged(value);
        },
        onFieldSubmitted: (value) {
          if (widget.onSubmitted != null) widget.onSubmitted!(value);
        },
      ),
    );
  }
}
