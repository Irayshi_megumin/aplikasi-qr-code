import 'package:belajar_firebase/auth.dart';
import 'package:belajar_firebase/widgets/form/password_field/password_field.dart';
import 'package:belajar_firebase/widgets/form/textfield/textfield.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:belajar_firebase/pages/home.dart';
import 'package:belajar_firebase/widgets/form/utils/validator/validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({
    Key? key,
    required this.role,
  }) : super(key: key);
  final String role;

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  String? email;
  String? password;
  String? nisn;
  String? name;
  String? mata_kuliah;

  Future<void> register(String email, String password, String name,
      {String mata_kuliah = '', String nisn = ''}) async {
    await Auth()
        .auth
        .createUserWithEmailAndPassword(email: email, password: password)
        .then((value) =>
            {postDetailsToFirestore(email, name, widget.role, mata_kuliah)})
        .catchError((e) {
      print('Error getting document: $e');
    });
  }

  postDetailsToFirestore(
      String email, String name, String role, String mata_kuliah) async {
    var user = Auth().auth.currentUser;
    CollectionReference ref = FirebaseFirestore.instance.collection('users');
    Map<String, dynamic> userData = {
      'email': email,
      'name': name,
      'role': role,
    };

    if (role == 'Guru') {
      userData['mata_kuliah'] = mata_kuliah;
    }

    ref.doc(user!.uid).set(userData);
    final docRef = ref.doc(FirebaseAuth.instance.currentUser!.uid);
    docRef.get().then(
      (DocumentSnapshot doc) {
        // final data = doc.data() as Map<String, dynamic>;
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => HomeScreen()));
      },
      onError: (e) => print("Error getting document: $e"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(197, 223, 248, 1),
      body: Center(
          child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        elevation: 2,
        margin: EdgeInsets.all(30),
        child: Container(
          width: MediaQuery.of(context).size.width * 1.5,
          height: MediaQuery.of(context).size.width * 1.5,
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                image: AssetImage('assets/images/register.png'),
                width: 150,
              ),
              SizedBox(
                height: 30,
              ),
              XTextField(
                label: "Email",
                prefixIcon: Icons.people,
                validator: Validator.required,
                onChanged: (value) {
                  email = value;
                  print(email);
                },
              ),
              XPasswordField(
                label: "Password",
                prefixIcon: Icons.key,
                validator: Validator.required,
                onChanged: (value) {
                  password = value;
                  print(password);
                },
              ),
              Visibility(
                visible: (widget.role == 'Siswa'),
                child: XTextField(
                  label: "Nisn",
                  prefixIcon: Icons.people,
                  validator: Validator.required,
                  onChanged: (value) {
                    nisn = value;
                    print(nisn);
                  },
                ),
              ),
              XTextField(
                label: "Nama Lengkap",
                prefixIcon: Icons.people,
                validator: Validator.required,
                onChanged: (value) {
                  name = value;
                  print(name);
                },
              ),
              Visibility(
                visible: (widget.role == 'Guru'),
                child: XTextField(
                  label: "Mata Kuliah",
                  prefixIcon: Icons.people,
                  validator: Validator.required,
                  onChanged: (value) {
                    mata_kuliah = value;
                    print(mata_kuliah);
                  },
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                      onPressed: () async {
                        try {
                          if (widget.role == 'Siswa') {
                            await register(email!, password!, name!,
                                nisn: nisn!);
                          }
                          if (widget.role == 'Guru') {
                            await register(email!, password!, name!,
                                mata_kuliah: mata_kuliah!);
                          }
                        } catch (e) {
                          final snackbar =
                              SnackBar(content: const Text('Register gagal'));
                          ScaffoldMessenger.of(context).showSnackBar(snackbar);
                        }
                      },
                      child: Text(
                        'Register',
                        style: TextStyle(fontSize: 16),
                      )),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Cancel',
                        style: TextStyle(fontSize: 16),
                      )),
                ],
              ),
            ],
          ),
        ),
      )),
    );
  }
}
