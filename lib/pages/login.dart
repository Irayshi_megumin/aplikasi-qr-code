import 'package:belajar_firebase/pages/home.dart';
import 'package:belajar_firebase/pages/register_screen.dart';
import 'package:belajar_firebase/auth.dart';
import 'package:belajar_firebase/widgets/form/textfield/textfield.dart';
import 'package:belajar_firebase/widgets/form/password_field/password_field.dart';
import 'package:belajar_firebase/widgets/form/utils/validator/validator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({
    Key? key,
    required this.role,
  }) : super(key: key);
  final String role;

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String? email;
  String? password;

  String? role;

  @override
  void initState() {
    super.initState();
    if (FirebaseAuth.instance.currentUser != null) {
      getLogin();
    }
  }

  Future getLogin() async {
    final docRef = Auth()
        .db
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser!.uid);
    docRef.get().then(
      (DocumentSnapshot doc) {
        final data = doc.data() as Map<String, dynamic>;
        role = data['role'];
        if (role == 'Guru') {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
        }
        if (role == 'Siswa') {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
        }
      },
      onError: (e) => print("Error getting document: $e"),
    );
  }

  Future login(String email, String password) async {
    await Auth()
        .auth
        .signInWithEmailAndPassword(email: email, password: password);
    final docRef = Auth()
        .db
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser!.uid);
    docRef.get().then(
      (DocumentSnapshot doc) {
        final data = doc.data() as Map<String, dynamic>;
        role = data['role'];
        if (role == 'Guru') {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomeScreen()));
        }
      },
      onError: (e) => print("Error getting document: $e"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(197, 223, 248, 1),
      body: Center(
          child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        elevation: 2,
        margin: EdgeInsets.all(30),
        child: Container(
          width: MediaQuery.of(context).size.width * 1.5,
          height: MediaQuery.of(context).size.width * 1.5,
          padding: EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Login',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
              ),
              SizedBox(
                height: 10,
              ),
              Image(
                image: AssetImage('assets/images/login.png'),
                width: 150,
              ),
              SizedBox(
                height: 35,
              ),
              XTextField(
                label: "Email",
                prefixIcon: Icons.people,
                validator: Validator.required,
                onChanged: (value) {
                  email = value;
                  print(email);
                },
              ),
              XPasswordField(
                label: "Password",
                prefixIcon: Icons.key,
                validator: Validator.required,
                onChanged: (value) {
                  password = value;
                  print(password);
                },
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  TextButton(
                      onPressed: () async {
                        try {
                          await login(email!, password!);
                        } catch (e) {
                          final snackbar =
                              SnackBar(content: const Text('Login gagal'));
                          ScaffoldMessenger.of(context).showSnackBar(snackbar);
                        }
                      },
                      child: Text(
                        'Masuk',
                        style: TextStyle(
                            color: Color.fromRGBO(4, 163, 236, 1),
                            fontSize: 16,
                            fontWeight: FontWeight.w700),
                      )),
                  Text(
                    'Lupa Password',
                    style: TextStyle(fontSize: 12),
                  )
                ],
              ),
              SizedBox(
                height: 30,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    (widget.role == 'Siswa')
                        ? 'Belum punya akun siswa? '
                        : 'Belum punya akun guru? ',
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 12,
                        fontWeight: FontWeight.w400),
                  ),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => RegisterScreen(
                                    role: (widget.role == 'Siswa')
                                        ? 'Siswa'
                                        : 'Guru')))
                            .then((value) {
                          setState(() {});
                        });
                      },
                      child: Text(
                        'Daftar',
                        style: TextStyle(
                            color: Color.fromRGBO(4, 163, 236, 1),
                            fontSize: 12,
                            fontWeight: FontWeight.w700),
                      )),
                ],
              )
            ],
          ),
        ),
      )),
    );
  }
}
