import 'package:belajar_firebase/auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../widgets/form/dropdown/dropdown_field.dart';
import '../widgets/form/textfield/textfield.dart';
import '../widgets/form/utils/validator/validator.dart';
import '../../core/res/color.dart';

class editJadwalMataKuliahScreen extends StatefulWidget {
  const editJadwalMataKuliahScreen({Key? key, required this.id})
      : super(key: key);

  final String id;

  @override
  State<editJadwalMataKuliahScreen> createState() =>
      _editJadwalMataKuliahScreenState();
}

class _editJadwalMataKuliahScreenState
    extends State<editJadwalMataKuliahScreen> {
  String? nameKelas;
  String? mata_kuliah;
  String? hari;
  String? jam_masuk;
  String? jam_keluar;

  List<Map<String, dynamic>> hariList = [
    {'label': 'Senin', 'value': 'Senin'},
    {'label': 'Selasa', 'value': 'Selasa'},
    {'label': 'Rabu', 'value': 'Rabu'},
    {'label': 'Kamis', 'value': 'Kamis'},
    {'label': 'Jumat', 'value': 'Jumat'},
  ];

  // Future createKelas(
  //     {required String nameKelas,
  //     required String mata_kuliah,
  //     required String hari,
  //     required String jam_masuk,
  //     jam_keluar}) async {
  //   print(nameKelas);
  //   print(hari);
  //   print(jam_masuk);
  //   print(jam_keluar);
  //   print(mata_kuliah);
  //   final docKelas = FirebaseFirestore.instance.collection('kelas').doc();

  //   final kelas = Kelas(
  //       name: nameKelas,
  //       mata_kuliah: mata_kuliah,
  //       hari: hari,
  //       jam_masuk: jam_masuk,
  //       jam_keluar: jam_keluar);
  //   final json = kelas.toJson();

  //   try {
  //     await docKelas.set(json);
  //     print('Berhasil');
  //   } catch (e) {
  //     print('Gagal: $e');
  //   }
  // }

  Future<List<String>> _getNameList(List<String> nisnList) async {
    List<String> nameList = [];

    for (String nisn in nisnList) {
      DocumentSnapshot userSnapshot =
          await FirebaseFirestore.instance.doc(nisn).get();
      if (userSnapshot.exists) {
        Map<String, dynamic> userData =
            userSnapshot.data() as Map<String, dynamic>;
        String name = userData['name'];
        nameList.add(name);
      }
    }

    return nameList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Jadwal Mata Kuliah"),
      ),
      body: _buildBody(context),
    );
  }

  StreamBuilder<DocumentSnapshot<Map<String, dynamic>>> _buildBody(
      BuildContext context) {
    return StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('kelas')
            .doc(widget.id)
            .snapshots(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Map<String, dynamic> data =
                snapshot.data!.data() as Map<String, dynamic>;

            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  XTextField(
                    label: "Nama Kelas",
                    value: data['name'],
                    validator: Validator.required,
                    onChanged: (value) {
                      nameKelas = value;
                      print(nameKelas);
                    },
                  ),
                  XTextField(
                    label: "Mata Pelajaran",
                    readOnly: true,
                    value: data['mata_kuliah'],
                    validator: Validator.required,
                    onChanged: (value) {
                      mata_kuliah = value;
                      print(mata_kuliah);
                    },
                  ),
                  Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: XDropdownField(
                          label: "Hari",
                          value: data['hari'],
                          validator: Validator.required,
                          items: hariList,
                          onChanged: (value, label) {
                            print("$value : $label");
                            hari = value;
                          },
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: XTextField(
                          label: "Jam Masuk",
                          value: data['jam_masuk'],
                          readOnly: true,
                          validator: Validator.required,
                          onTap: true,
                          onChanged: (value) {
                            jam_masuk = value;
                            print('Test: $jam_masuk');
                          },
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: XTextField(
                          label: "Jam Keluar",
                          value: data['jam_keluar'],
                          readOnly: true,
                          validator: Validator.required,
                          onTap: true,
                          onChanged: (value) {
                            jam_keluar = value;
                            print('Test: $jam_keluar');
                          },
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              padding: const EdgeInsets.all(20.0),
                              elevation: 2,
                              backgroundColor: Colors.pink,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20))),
                          onPressed: () {
                            // print(nameKelas);
                            // print(hari);
                            // print(jam);
                            // print(mata_kuliah);
                            try {
                              final docKelas = FirebaseFirestore.instance
                                  .collection('kelas')
                                  .doc(widget.id);
                              docKelas.update({
                                "name": nameKelas,
                                "hari": hari,
                                "jam_masuk": jam_masuk,
                                "jam_keluar": jam_keluar
                              });
                              final snackbar = SnackBar(
                                  content: const Text('Edit Data Berhasil'));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackbar);
                            } catch (e) {
                              print('Gagal: $e');
                              final snackbar = SnackBar(
                                  content: const Text('Edit Data Gagal'));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackbar);
                            }
                          },
                          child: Text(
                            'Edit Jadwal',
                            style: GoogleFonts.raleway(
                              textStyle: const TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 2),
                            ),
                          ),
                        ),
                        OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            padding: const EdgeInsets.all(20.0),
                            foregroundColor: Colors.pink,
                            side: BorderSide(
                              color: Colors.pink,
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                          ),
                          onPressed: () {
                            try {
                              final docKelas = FirebaseFirestore.instance
                                  .collection('kelas')
                                  .doc(widget.id);
                              docKelas.delete();
                              Navigator.of(context, rootNavigator: false).pop();
                              final snackbar = SnackBar(
                                  content: const Text('Hapus Data Berhasil'));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackbar);
                            } catch (e) {
                              print('Gagal: $e');
                              final snackbar = SnackBar(
                                  content: const Text('Edit Data Gagal'));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackbar);
                            }
                          },
                          child: Text(
                            'Hapus Jadwal',
                            style: GoogleFonts.raleway(
                              textStyle: const TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  letterSpacing: 2),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Daftar Siswa",
                        style: TextStyle(
                          color: Colors.blueGrey[900],
                          fontWeight: FontWeight.w700,
                          fontSize: 22,
                        ),
                      ),
                      const Spacer(),
                      InkWell(
                        onTap: () {
                          dialogFormPengguna(false, null);
                        },
                        child: Text(
                          "Tambah Siswa",
                          style: TextStyle(
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      )
                    ],
                  ),
                  SingleChildScrollView(
                    child: StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('kelas')
                          .where('id_guru',
                              isEqualTo: FirebaseAuth.instance.currentUser!.uid)
                          .where('id', isEqualTo: widget.id)
                          .snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (snapshot.hasError) {
                          return Text('Error: ${snapshot.error}');
                        }

                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Text('Loading...');
                        }

                        if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                          return Text('Document does not exist');
                        }

                        // Mengambil semua dokumen dari snapshot
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        List<String> nisnList = [];

                        for (DocumentSnapshot doc in documents) {
                          List<dynamic> daftarSiswa =
                              doc['daftar_siswa'] as List<dynamic>;
                          for (dynamic siswa in daftarSiswa) {
                            Map<String, dynamic> siswaData =
                                siswa as Map<String, dynamic>;
                            String nisn = siswaData['nisn'].path;
                            nisnList.add(nisn);
                          }
                        }

                        // Menggunakan data yang diambil
                        return StreamBuilder<List<String>>(
                          stream: _getNameList(nisnList).asStream(),
                          builder: (BuildContext context,
                              AsyncSnapshot<List<String>> snapshot) {
                            if (snapshot.hasError) {
                              return Text('Error: ${snapshot.error}');
                            }

                            if (snapshot.connectionState ==
                                ConnectionState.waiting) {
                              return Text('Loading...');
                            }

                            if (!snapshot.hasData || snapshot.data!.isEmpty) {
                              return Text('Document does not exist');
                            }

                            // Menggunakan data name
                            List<String> nameList = snapshot.data!;
                            return SizedBox(
                              height: 200,
                              child: ListView.builder(
                                itemCount: nameList.length,
                                itemBuilder: (BuildContext context, int index) {
                                  String name = nameList[index];
                                  return Container(
                                    child: InkWell(
                                      // onTap: () => Navigator.of(context).push(
                                      //     MaterialPageRoute(
                                      //         builder: (context) =>
                                      //             editJadwalMataKuliahScreen(
                                      //               id: data['id'],
                                      //             ))),
                                      child: buildContactRow(name),
                                    ),
                                  );
                                },
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),

                  // StreamBuilder(
                  //     stream: readKelas(),
                  //     builder: (context, snapshot) {
                  //       if (snapshot.hasData) {
                  //         final kelas = snapshot.data!;

                  //         return ListView(
                  //           children: kelas.map(buildKelas).toList(),
                  //         );
                  //       } else {
                  //         return Center(
                  //           child: CircularProgressIndicator(),
                  //         );
                  //       }
                  //     }),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else {
            return Text('Loading...');
          }
        });
  }

  void dialogFormPengguna(bool kondisi, dynamic value) async {
    String nisn = '';

    if (value != null) {
      nisn = value['nisn'];
    }

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text((value == null)
                    ? 'Tambah Data'
                    : (kondisi)
                        ? 'Baca Data'
                        : 'Edit Data'),
              ],
            ),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  XTextField(
                    label: "Nisn Siswa",
                    validator: Validator.required,
                    onChanged: (value) {
                      nisn = value;
                      print(nisn);
                    },
                  ),
                ],
              ),
            ),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context),
                child: Text('Tutup'),
              ),
              TextButton(
                onPressed: () async {
                  if (value == null) {
                    // createSiswa(nisn: nisn);
                    final docSiswa = FirebaseFirestore.instance
                        .collection('kelas')
                        .doc('I8lL7Xg2tzfyQJDFZfzM');
                    docSiswa.update({
                      "mata_kuliah.$mata_kuliah": FieldValue.arrayUnion([nisn]),
                    });
                    Navigator.of(context).pop('dialog');
                    setState(() {});
                  }
                  if (kondisi) {
                    // Read
                    Navigator.of(context, rootNavigator: false).pop('dialog');
                    dialogFormPengguna(false, value);
                  } else {
                    // Update
                    Navigator.of(context, rootNavigator: false).pop('dialog');
                    setState(() {});
                  }
                },
                child: (kondisi)
                    ? Text('Edit')
                    : (value == null)
                        ? Text('Tambah')
                        : Text('Save'),
              ),
            ],
          );
        });
  }

  Container buildContactRow(String name) {
    String firstName = name.split(' ')[0];
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.grey.shade200),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              CircleAvatar(
                child: Text(
                  firstName,
                ),
                radius: 25,
              ),
              SizedBox(
                width: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 4,
                    child: Text(
                      name,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}

class Kelas {
  String? name;
  String? mata_kuliah;
  String? hari;
  String jam_masuk;
  String jam_keluar;

  Kelas({
    required this.name,
    required this.mata_kuliah,
    required this.hari,
    required this.jam_masuk,
    required this.jam_keluar,
  });

  Map<String, dynamic> toJson() => {
        'name': name,
        'mata_kuliah': mata_kuliah,
        'hari': hari,
        'jam_masuk': jam_masuk,
        'jam_keluar': jam_keluar
      };

  static Kelas fromJson(Map<String, dynamic> json) => Kelas(
      name: json['name'],
      mata_kuliah: json['mata_kuliah'],
      hari: json['hari'],
      jam_masuk: json['jam_masuk'],
      jam_keluar: json['jam_keluar']);
}

class Siswa {
  String? nisn;

  Siswa({
    required this.nisn,
  });

  Map<String, dynamic> toJson() => {'nisn': nisn};

  static Siswa fromJson(Map<String, dynamic> json) => Siswa(nisn: json['nisn']);
}
