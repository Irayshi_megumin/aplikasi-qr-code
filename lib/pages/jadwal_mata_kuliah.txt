import 'package:flutter/material.dart';
import '../pages/createjadwal.dart';
import '../models/task.dart';
import '../widgets/card.dart';

class JadwalMataKuliahScreen extends StatefulWidget {
  const JadwalMataKuliahScreen({Key? key}) : super(key: key);

  @override
  State<JadwalMataKuliahScreen> createState() => _JadwalMataKuliahScreenState();
}

class _JadwalMataKuliahScreenState extends State<JadwalMataKuliahScreen> {
  final List<CardModel> _jadwalList = [
    CardModel(cardName: "Senin"),
    CardModel(cardName: "Selasa"),
    CardModel(cardName: "Rabu"),
    CardModel(cardName: "Kamis"),
    CardModel(cardName: "Jumat"),
    CardModel(cardName: "Sabtu"),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Jadwal Mata Kuliah"),
      ),
      body: _buildBody(context),
    );
  }

  Container _buildBody(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 10,
          ),
          Column(
            children: _jadwalList
                .map(
                  (e) => CardWidget(
                    navigator: createJadwalMataKuliahScreen(),
                    cardModel: e,
                  ),
                )
                .toList(),
          )
        ],
      ),
    );
  }
}
