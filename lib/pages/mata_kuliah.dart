import 'package:belajar_firebase/globals.dart' as globals;
import 'package:belajar_firebase/pages/create_jadwal.dart';
import 'package:belajar_firebase/pages/edit_jadwal.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../widgets/circle_gradient_icon.dart';

class MataKuliahScreen extends StatefulWidget {
  const MataKuliahScreen({
    Key? key,
  }) : super(key: key);

  @override
  _MataKuliahScreenState createState() => _MataKuliahScreenState();
}

class _MataKuliahScreenState extends State<MataKuliahScreen> {
  List? hari = [
    'Senin',
    'Selasa',
    'Rabu',
    'Kamis',
    'Jumat',
  ];
  List? data;
  String searchValue = '';

  @override
  // void initState() {
  //   super.initState;
  //   getData();
  // }

//   void getData() {
//     final docRef = Auth()
//         .db
//         .collection("users")
//         .doc(FirebaseAuth.instance.currentUser!.uid);
//     docRef.get().then(
//       (DocumentSnapshot doc) {
//         final data = doc.data() as Map<String, dynamic>;
//         this.data = data as List?;
//       },
//       onError: (e) => print("Error getting document: $e"),
//     );
// }

  // void getOutlet() async {
  //   var response = await readOutlet();

  //   setState(() {
  //     this._list = response as List;
  //   });
  // } CircularProgressIndicator()

  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: hari!.length,
      child: Builder(builder: (BuildContext context) {
        final TabController tabController = DefaultTabController.of(context);
        tabController.addListener(() {
          if (!tabController.indexIsChanging) {
            tabController.index;
            // print(tabController.index);
            setState(() {});
          }
        });
        return _body(tabController.index);
      }),
    );
  }

  Scaffold _body([int? index]) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        iconTheme: IconThemeData(
          color: Colors.white,
        ),
        title: const Text('Jadwal Mata Kuliah',
            style: TextStyle(color: Colors.white)),
        bottom: TabBar(
            tabs: hari!
                .map((e) => Tab(
                      text: e,
                    ))
                .toList()),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // dialogFormPengguna(false, null); // crud tambah
        },
        tooltip: 'Tambah Data Kelas',
        backgroundColor: Colors.lightBlue,
        child: CircleGradientIcon(
          color: Colors.pink,
          onTap: () async {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return createJadwalMataKuliahScreen(
                    hari: hari![index!],
                  );
                },
              ),
            );
          },
          size: 60,
          iconSize: 30,
          icon: Icons.add,
        ),
      ),
      body: TabBarView(
        children: hari!
            .map(
              (e) => Container(
                padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 30.0),
                width: MediaQuery.of(context).size.width,
                child: ListView(
                  children: [
                    Text(
                      "Daftar Kelas",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(vertical: 8),
                      child: TextField(
                        decoration: InputDecoration(
                          prefixIcon:
                              Icon(Icons.search, size: 30, color: Colors.grey),
                          hintText: "Search",
                          hintStyle: TextStyle(color: Colors.grey),
                        ),
                        onChanged: (value) {
                          setState(() {
                            searchValue = value;
                          });
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    StreamBuilder<QuerySnapshot>(
                      stream: FirebaseFirestore.instance
                          .collection('kelas')
                          .where('hari', isEqualTo: hari![index!])
                          .where('name',
                              isEqualTo:
                                  searchValue.isNotEmpty ? searchValue : null)
                          .snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (snapshot.hasError) {
                          return Text('Error: ${snapshot.error}');
                        }

                        if (snapshot.connectionState ==
                            ConnectionState.waiting) {
                          return Text('Loading...');
                        }

                        if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                          return Text('Document does not exist');
                        }

                        // Mengambil semua dokumen dari snapshot
                        List<DocumentSnapshot> documents = snapshot.data!.docs;

                        print(hari![index!]);

                        // Menggunakan data yang diambil
                        return ListView.builder(
                          shrinkWrap: true,
                          itemCount: documents.length,
                          itemBuilder: (context, index) {
                            Map<String, dynamic> data =
                                documents[index].data() as Map<String, dynamic>;
                            return InkWell(
                              onTap: () => Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          editJadwalMataKuliahScreen(
                                            id: data['id'],
                                          ))),
                              child: buildContactRow(
                                  data['name'],
                                  data['mata_kuliah'],
                                  data['jam_masuk'],
                                  data['jam_keluar']),
                            );
                          },
                        );
                      },
                    ),
                  ],
                ),
              ),
            )
            .toList(),
      ),
    );
  }

  Container buildContactRow(
      String name, String mata_kuliah, String jam_masuk, String jam_keluar) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.grey.shade200),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 4,
                    child: Text(
                      name,
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width / 2,
                    child: Text(
                      'Pelajaran: $mata_kuliah ($jam_masuk - $jam_keluar)',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Text(
                        'Hadir: 0',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        'Telat: 0',
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2,
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
