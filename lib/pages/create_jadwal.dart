import 'package:belajar_firebase/globals.dart' as globals;
import 'package:belajar_firebase/pages/edit_jadwal.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../widgets/form/dropdown/dropdown_field.dart';
import '../widgets/form/textfield/textfield.dart';
import '../widgets/form/utils/validator/validator.dart';

class createJadwalMataKuliahScreen extends StatefulWidget {
  const createJadwalMataKuliahScreen({Key? key, required this.hari})
      : super(key: key);

  final String hari;

  @override
  State<createJadwalMataKuliahScreen> createState() =>
      _createJadwalMataKuliahScreenState();
}

class _createJadwalMataKuliahScreenState
    extends State<createJadwalMataKuliahScreen> {
  String? nameKelas;
  String? mata_kuliah = globals.users['mata_kuliah'];
  String? hari;
  String? jam_masuk;
  String? jam_keluar;

  @override
  void initState() {
    super.initState();
    hari = widget.hari;
  }

  List<Map<String, dynamic>> hariList = [
    {'label': 'Senin', 'value': 'Senin'},
    {'label': 'Selasa', 'value': 'Selasa'},
    {'label': 'Rabu', 'value': 'Rabu'},
    {'label': 'Kamis', 'value': 'Kamis'},
    {'label': 'Jumat', 'value': 'Jumat'},
  ];

  Stream<List<Kelas>> readKelas() => FirebaseFirestore.instance
      .collection('kelas')
      .snapshots()
      .map((event) => event.docs.map((e) => Kelas.fromJson(e.data())).toList());

  Future createKelas(Kelas kelas) async {
    // print(nameKelas);
    // print(hari);
    // print(jam);
    // print(mata_kuliah);
    final docKelas = FirebaseFirestore.instance.collection('kelas').doc();
    kelas.id = docKelas.id;
    Map<String, dynamic> json = kelas.toJson();
    json['id_guru'] = FirebaseAuth.instance.currentUser!.uid;

    try {
      await docKelas.set(json);
      print('Berhasil');
    } catch (e) {
      print('Gagal: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Buat Jadwal Mata Kuliah"),
      ),
      body: _buildBody(context),
    );
  }

  Widget buildKelas(Kelas kelas) => ListTile(
        leading: CircleAvatar(child: Text(kelas.name!)),
        title: Text(kelas.hari!),
        subtitle: Text(kelas.mata_kuliah!),
      );

  Container _buildBody(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 10,
          ),
          XTextField(
            label: "Nama Kelas",
            validator: Validator.required,
            onChanged: (value) {
              nameKelas = value;
              print(nameKelas);
            },
          ),
          XTextField(
            label: "Mata Pelajaran",
            readOnly: true,
            value: globals.users['mata_kuliah'],
            validator: Validator.required,
            onChanged: (value) {
              mata_kuliah = value;
              print(mata_kuliah);
            },
          ),
          Row(
            children: [
              Flexible(
                flex: 1,
                child: XDropdownField(
                  label: "Hari",
                  value: widget.hari,
                  validator: Validator.required,
                  items: hariList,
                  onChanged: (value, label) {
                    print("$value : $label");
                    hari = value;
                  },
                ),
              ),
              Flexible(
                flex: 1,
                child: XTextField(
                  label: "Jam Masuk",
                  readOnly: true,
                  validator: Validator.required,
                  onTap: true,
                  onChanged: (value) {
                    jam_masuk = value;
                    print('Test: $jam_masuk');
                  },
                ),
              ),
              Flexible(
                flex: 1,
                child: XTextField(
                  label: "Jam Keluar",
                  readOnly: true,
                  validator: Validator.required,
                  onTap: true,
                  onChanged: (value) {
                    jam_keluar = value;
                    print('Test: $jam_keluar');
                  },
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.all(20.0),
                  elevation: 2,
                  backgroundColor: Colors.pink,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              onPressed: () {
                // print(nameKelas);
                // print(hari);
                // print(jam);
                // print(mata_kuliah);
                try {
                  final kelas = Kelas(
                      name: nameKelas!,
                      mata_kuliah: mata_kuliah!,
                      hari: hari!,
                      jam_masuk: jam_masuk!,
                      jam_keluar: jam_keluar!);
                  createKelas(kelas);
                  print(kelas.id);
                  final snackbar =
                      SnackBar(content: const Text('Tambah Data Berhasil'));
                  ScaffoldMessenger.of(context).showSnackBar(snackbar);
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => editJadwalMataKuliahScreen(
                            id: kelas.id,
                          )));
                } catch (e) {
                  print('Gagal: $e');
                  final snackbar =
                      SnackBar(content: const Text('Tambah Data Gagal'));
                  ScaffoldMessenger.of(context).showSnackBar(snackbar);
                }
              },
              child: Text(
                'Buat Jadwal',
                style: GoogleFonts.raleway(
                  textStyle: const TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2),
                ),
              ),
            ),
          ),
          // StreamBuilder(
          //     stream: readKelas(),
          //     builder: (context, snapshot) {
          //       if (snapshot.hasData) {
          //         final kelas = snapshot.data!;

          //         return ListView(
          //           children: kelas.map(buildKelas).toList(),
          //         );
          //       } else {
          //         return Center(
          //           child: CircularProgressIndicator(),
          //         );
          //       }
          //     }),
        ],
      ),
    );
  }
}

class Kelas {
  String id;
  String name;
  String mata_kuliah;
  String hari;
  String jam_masuk;
  String jam_keluar;

  Kelas({
    this.id = '',
    required this.name,
    required this.mata_kuliah,
    required this.hari,
    required this.jam_masuk,
    required this.jam_keluar,
  });

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'mata_kuliah': mata_kuliah,
        'hari': hari,
        'jam_masuk': jam_masuk,
        'jam_keluar': jam_keluar
      };

  static Kelas fromJson(Map<String, dynamic> json) => Kelas(
      id: json['id'],
      name: json['name'],
      mata_kuliah: json['mata_kuliah'],
      hari: json['hari'],
      jam_masuk: json['jam_masuk'],
      jam_keluar: json['jam_keluar']);
}
