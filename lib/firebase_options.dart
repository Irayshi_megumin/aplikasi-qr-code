// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyBHkRRjaMOsVVeJq3H4pPOdNLMvuKM_-k8',
    appId: '1:769483915141:web:393897c9c05ec7c5e34eec',
    messagingSenderId: '769483915141',
    projectId: 'belajar-firebase-59b67',
    authDomain: 'belajar-firebase-59b67.firebaseapp.com',
    storageBucket: 'belajar-firebase-59b67.appspot.com',
    measurementId: 'G-DNF612GYB2',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyBTBwMh9hHoCCg3IO2EMzTUR8RRpdMVJUM',
    appId: '1:769483915141:android:702eeb499eab3a71e34eec',
    messagingSenderId: '769483915141',
    projectId: 'belajar-firebase-59b67',
    storageBucket: 'belajar-firebase-59b67.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyAiENBkdJ1Devx1tokDHWKDsjD7iZwgJQk',
    appId: '1:769483915141:ios:3f6eae3c268b3529e34eec',
    messagingSenderId: '769483915141',
    projectId: 'belajar-firebase-59b67',
    storageBucket: 'belajar-firebase-59b67.appspot.com',
    iosClientId: '769483915141-mo4236ckl4vs1namu94hustvaq9bi4ij.apps.googleusercontent.com',
    iosBundleId: 'com.example.belajarFirebase',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyAiENBkdJ1Devx1tokDHWKDsjD7iZwgJQk',
    appId: '1:769483915141:ios:f90cb925eda0b0e0e34eec',
    messagingSenderId: '769483915141',
    projectId: 'belajar-firebase-59b67',
    storageBucket: 'belajar-firebase-59b67.appspot.com',
    iosClientId: '769483915141-bmd4kica801o325qrpg6eoimo57ol85n.apps.googleusercontent.com',
    iosBundleId: 'com.example.belajarFirebase.RunnerTests',
  );
}
