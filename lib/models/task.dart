class TaskModel {
  final DateTime from;
  final DateTime to;
  final String taskName;

  TaskModel({required this.from, required this.to, required this.taskName});
}

class CardModel {
  final String cardName;

  CardModel({required this.cardName});
}
