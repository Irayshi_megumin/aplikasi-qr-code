import 'package:flutter/material.dart';
import '../../pages/jadwal_mata_kuliah.dart';
import '../../pages/createjadwal.dart';
import '../../pages/login.dart';
import '../../pages/home.dart';
import '../../pages/today_task.dart';

class Routes {
  static const login = "/";
  static const home = "/home";
  static const todaysTask = "/task/todays";
  static const jadwalMataKuliah = "/jadwalmatakuliah";
  static const createJadwal = "/jadwalmatakuliah/create";
}

class RouterGenerator {
  static Route<dynamic> generateRoutes(RouteSettings settings) {
    switch (settings.name) {
      case Routes.login:
        return MaterialPageRoute(
          builder: ((context) => LoginScreen()),
        );
      case Routes.home:
        return MaterialPageRoute(
          builder: ((context) => const HomeScreen()),
        );
      case Routes.todaysTask:
        return MaterialPageRoute(
          builder: ((context) => const TodaysTaskScreen()),
        );
      case Routes.jadwalMataKuliah:
        return MaterialPageRoute(
          builder: ((context) => const JadwalMataKuliahScreen()),
        );
      case Routes.createJadwal:
        return MaterialPageRoute(
          builder: ((context) => const createJadwalMataKuliahScreen()),
        );
      default:
        return MaterialPageRoute(
          builder: ((context) => const HomeScreen()),
        );
    }
  }
}
